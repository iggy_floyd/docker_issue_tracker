#!/bin/bash

echo "_docker_deployment_redmine=$_docker_deployment_redmine"
sleep 10
[[ "${_docker_deployment_redmine}" == "import" ]] && /install_redmine.sh


#eval $'cat <<\002\n'"$(</ngnix_forward_proxy)"$'\n\002' > /etc/nginx/sites-enabled/default
#service nginx restart
/etc/init.d/postgresql restart

/usr/sbin/sshd

# for testing purpose: standalone web server to test the redmine
cd /var/lib/redmine
ruby1.8 bin/rails server webrick -e production -b 0.0.0.0 -p 3000

