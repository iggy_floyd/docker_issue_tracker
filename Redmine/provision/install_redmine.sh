#!/bin/bash


# install tools needed by ruby
#sudo apt-get install -y  gcc make build-essential linux-headers-$(uname -r | sed -e 's/-amd64//')
sudo apt-get install -y  make 

# install ruby and gems 
sudo apt-get install  software-properties-common -y
sudo apt-add-repository ppa:brightbox/ruby-ng -y
sudo apt-get update 
sudo apt-get install  ruby1.9.3 -y
ln -s /usr/bin/gem /usr/bin/gem1.8 
ln -s /usr/bin/ruby /usr/bin/ruby1.8

gem install bundler
#sudo apt-get install rubygems -y

echo "
#!/usr/bin/ruby1.8

require 'rubygems'

version = ">= 0"
if ARGV.first =~ /^_(.*)_$/ and Gem::Version.correct? \$1 then
  version = \$1
  ARGV.shift
end

gem 'bundler', version
load Gem.bin_path('bundler', 'bundle', version)

" > /usr/bin/bundle
chmod +x /usr/bin/bundle

# install redmine
git clone git://github.com/redmine/redmine.git
mv redmine/ /var/lib/redmine

#svn co https://svn.redmine.org/redmine/branches/3.2-stable redmine-3.2
#mv redmine-3.2/ /var/lib/redmine
cd /var/lib/redmine
git checkout 3.2-stable

/prepare_db.sh


echo "production:
 adapter: postgresql
 database: redmine
 host: localhost
 username: redmine
 password: redmine
 encoding: utf8
 schema_search_path: public" > config/database.yml

bundle install --without development test mysql sqlite

export PATH=$PATH:$HOME/bin:/var/lib/gems/1.8/bin
RAILS_ENV=production rake generate_secret_token
RAILS_ENV=production rake db:migrate
RAILS_ENV=production rake  REDMINE_LANG=en redmine:load_default_data

ln -s /var/lib/redmine/public /var/www/redmine
chown -R www-data:www-data /var/www/redmine
