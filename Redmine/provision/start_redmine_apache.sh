#!/bin/bash


echo "_docker_deployment_redmine=$_docker_deployment_redmine"
sleep 10
[[ "${_docker_deployment_redmine}" == "import" ]] && /install_redmine.sh


#eval $'cat <<\002\n'"$(</ngnix_forward_proxy)"$'\n\002' > /etc/nginx/sites-enabled/default
#service nginx restart
/etc/init.d/postgresql restart


echo "

<IfModule mod_passenger.c>
  PassengerRoot /usr/lib/ruby/vendor_ruby/phusion_passenger/locations.ini
  PassengerDefaultRuby /usr/bin/ruby
  PassengerDefaultUser www-data
  PassengerRuby /usr/bin/ruby
</IfModule>

"  > /etc/apache2/mods-available/passenger.conf 


echo "

<VirtualHost *:80>

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined


        ServerName redmine.example.com
        DocumentRoot /var/www/redmine

       <Directory /var/www/redmine>
         AllowOverride all
         Options -MultiViews
         RailsBaseURI /redmine
         PassengerResolveSymlinksInDocumentRoot on
        </Directory>

</VirtualHost>

" > /etc/apache2/sites-available/000-default.conf


chmod 0755 /var/lib/redmine/public
chown www-data:www-data  /var/lib/redmine/Gemfile.lock

sudo service apache2 restart
/usr/sbin/sshd -D
