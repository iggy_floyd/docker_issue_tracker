#!/bin/bash

service postgresql start && \
  pg_dropcluster --stop 9.3 main || echo "Some problems encountered!"
service postgresql start && \
  pg_createcluster --start -e UTF-8 9.3 main || echo "Some problems encountered!"


sudo -u postgres createuser --no-superuser --no-createrole --createdb redmine
sudo -u postgres createdb  --template=template0   --locale=en_US.UTF-8 -E UTF-8  -O redmine  redmine

echo "ALTER USER redmine WITH PASSWORD 'redmine';" | sudo -u postgres psql -d "redmine"


echo "host all  all    0.0.0.0/0  trust" >> /etc/postgresql/9.3/main/pg_hba.conf
echo "local all  all     md5" >> /etc/postgresql/9.3/main/pg_hba.conf
echo "listen_addresses='*'" >> /etc/postgresql/9.3/main/postgresql.conf

echo "localhost:5432:redmine:redmine:redmine">$HOME/.pgpass
chmod 0600 $HOME/.pgpass
/etc/init.d/postgresql restart


chown -R postgres:postgres /var/run/postgresql
chown -R postgres:postgres /etc/postgresql/9.3

