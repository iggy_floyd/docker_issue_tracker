#!/bin/bash

/prepare_db.sh

virtualenv /opt/trac/bhenv
source /opt/trac/bhenv/bin/activate
pip install psycopg2 
pip install trac
#pip install http://download.edgewall.org/trac/Trac-1.1.6.tar.gz

# add git repository 
mkdir -p /var/repos/addr
cat <<_END >/var/repos/addr/README.rst

Address Validator Projects
==================================

This is a template of the documentation page

_END

cd /var/repos/addr/
git init
git add .

# adding hooks
cat  <<_END >.git/hooks/post-commit
#!/bin/sh 

. /opt/trac/bhenv/bin/activate

REV=\$(git rev-parse HEAD)
trac-admin /opt/trac/trac_env/ changeset added addr \$REV

_END


cat  <<_END >.git/hooks/post-revprop-change
#!/bin/sh 

. /opt/trac/bhenv/bin/activate
REV=\$(git rev-parse HEAD)
trac-admin /opt/trac/trac_env/ changeset modified addr \$REV

_END

chmod 077 .git/hooks/post-commit
chmod 077 .git/hooks/post-revprop-change


cd -

#trac-admin   /opt/trac/trac_env/ initenv 'example' "sqlite:db/trac.db" "/svn/test" svn "/usr/local/python/share/trac/templates"
trac-admin    /opt/trac/trac_env/ initenv 'main' 'postgres://trac:trac@localhost:5432/trac' git "/var/repos/addr/.git"  --inherit=/base.ini
trac-admin   /opt/trac/trac_env permission  add anonymous TRAC_ADMIN
cp -r /opt/trac/bhenv/local/lib/python2.7/site-packages/trac/ticket/templates/*  /opt/trac/trac_env/templates/

cd /var/repos/addr/

git config --global user.email "iggy.floyd.de@gmail.com"
git config --global user.name "i.marfin"
git commit -am "Initialization"

cd -
