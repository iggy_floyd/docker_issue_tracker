#!/bin/bash



echo "_docker_deployment_trac=$_docker_deployment_trac"
sleep 5
[[ "${_docker_deployment_trac}" == "import" ]] && /install_trac.sh


source /opt/trac/bhenv/bin/activate

eval $'cat <<\002\n'"$(</ngnix_forward_proxy)"$'\n\002' > /etc/nginx/sites-enabled/default
service nginx restart
/etc/init.d/postgresql restart

/usr/sbin/sshd
#sudo -u trac tracd --port=8080 /opt/trac/trac_env/main
tracd --port=8080 /opt/trac/trac_env
