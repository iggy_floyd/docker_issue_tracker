#!/bin/bash

echo "Installing additional dependent packages"
sudo apt-get update

sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo sh -c 'echo deb "http://apt.dockerproject.org/repo debian-wheezy main" >> /etc/apt/sources.list'
sudo apt-get update
sudo apt-get install -y docker-engine curl wget git git-core
sudo service docker start
sudo docker run hello-world

# installation of the docker-compose
sudo sh -c "curl -L https://github.com/docker/compose/releases/download/1.5.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose"
sudo chmod +x /usr/local/bin/docker-compose


# mkdir needed folders
mkdir /home/vagrant/log_postgresql/; chmod 0777 /home/vagrant/log_postgresql/
mkdir /home/vagrant/etc_postgresql/; chmod 0777 /home/vagrant/etc_postgresql/
mkdir /home/vagrant/lib_postgresql/; chmod 0777 /home/vagrant/lib_postgresql/

