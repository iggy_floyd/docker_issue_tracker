#!/bin/bash


wget  http://mirror.yannic-bonenberger.com/apache/bloodhound/apache-bloodhound-0.8.tar.gz
tar  xvzf apache-bloodhound-0.8.tar.gz
cd apache-bloodhound-0.8/installer
virtualenv /opt/bloodhound/bhenv
source /opt/bloodhound/bhenv/bin/activate
pip install -r requirements.txt
pip install psycopg2

/prepare_db.sh
python bloodhound_setup.py -u bloodhound -p bloodhound --admin-user=admin --database-name=bloodhound --admin-password=admin --environments_directory=/opt/bloodhound/tracker_env/ -d postgres --default-product-prefix=ADR
#cp /base.ini /opt/bloodhound/tracker_env/main/conf/base.ini
#cp /trac.ini /opt/bloodhound/tracker_env/main/conf/trac.ini

# add git repository 
mkdir -p /var/repos/addr
cat <<_END >/var/repos/addr/README.rst



Address Validator Projects
==================================

This is a template of the documentation page

_END

cd /var/repos/addr/
git init
git add .

# adding hooks
cat  <<_END >.git/hooks/post-commit
#!/bin/sh 

. /opt/bloodhound/bhenv/bin/activate

REV=\$(git rev-parse HEAD)
/opt/bloodhound/bhenv/bin/trac-admin /opt/bloodhound/tracker_env/main/ changeset added addr \$REV

_END

cat  <<_END >.git/hooks/post-revprop-change
#!/bin/sh 

. /opt/bloodhound/bhenv/bin/activate
REV=\$(git rev-parse HEAD)
/opt/bloodhound/bhenv/bin/trac-admin /opt/bloodhound/tracker_env/main/ changeset modified addr \$REV

_END

chmod 077 .git/hooks/post-commit
chmod 077 .git/hooks/post-revprop-change

#git config --global user.email "iggy.floyd.de@gmail.com"
#git config --global user.name "i.marfin"
#git commit -am "Initialization"

cd -
