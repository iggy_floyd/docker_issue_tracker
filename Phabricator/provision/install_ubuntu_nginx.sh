#!/bin/bash

confirm() {
  echo "Press RETURN to continue, or ^C to cancel.";
  read -e ignored
}

# set up MTA
/dockerscriptetchosts.sh
/etc/init.d/postfix restart


GIT='git'

LTS="Ubuntu 14.04"
ISSUE=`cat /etc/issue`
if [[ $ISSUE != Ubuntu* ]]
then
  echo "This script is intended for use on Ubuntu, but this system appears";
  echo "to be something else. Your results may vary.";
  echo
#  confirm
elif [[ `expr match "$ISSUE" "$LTS"` -eq ${#LTS} ]]
then
  GIT='git-core'
fi

echo "PHABRICATOR UBUNTU INSTALL SCRIPT";
echo "This script will install Phabricator and all of its core dependencies.";
echo "Run it from the directory you want to install into.";
echo

ROOT=`pwd`
echo "Phabricator will be installed to: ${ROOT}.";
#confirm

echo "Testing sudo..."
sudo true
if [ $? -ne 0 ]
then
  echo "ERROR: You must be able to sudo to run this script.";
  exit 1;
fi;

echo "Installing dependencies: git, apache, mysql, php...";
echo

set +x


# Enable mod_rewrite
#sudo a2enmod rewrite
export TERM=xterm

sudo apt-get -qq update
echo "mysql-server-5.5 mysql-server/root_password password root" | debconf-set-selections
echo "mysql-server-5.5 mysql-server/root_password_again password root" | debconf-set-selections
apt-get -y install mysql-server-5.5 $GIT dpkg-dev \
  php5 php5-mysql php5-gd php5-dev php5-curl php-apc php5-cli php5-json php5-fpm 

echo "postfix postfix/mailname string your.hostname.com" | debconf-set-selections
echo "postfix postfix/main_mailer_type string 'Internet Site'" | debconf-set-selections
apt-get -y install postfix
apt-get -y install mailutils



if [ ! -e libphutil ]
then
  git clone https://github.com/phacility/libphutil.git
else
  (cd libphutil && git pull --rebase)
fi

if [ ! -e arcanist ]
then
  git clone https://github.com/phacility/arcanist.git
else
  (cd arcanist && git pull --rebase)
fi

if [ ! -e phabricator ]
then
  git clone https://github.com/phacility/phabricator.git
else
  (cd phabricator && git pull --rebase)
fi



HAVEPCNTL=`php -r "echo extension_loaded('pcntl');"`
if [ $HAVEPCNTL != "1" ]
then
  echo "Installing pcntl...";
  echo
  apt-get source php5
  PHP5=`ls -1F | grep '^php5-.*/$'`
  (cd $PHP5/ext/pcntl && phpize && ./configure && make && sudo make install)
else
  echo "pcntl already installed";
fi

/etc/init.d/php5-fpm restart

#echo "127.0.0.1       phabricator.localhost" >>  /etc/hosts
sudo sh -c 'echo  "127.0.0.1       phabricator.localhost" >> /etc/hosts'
ln -s /phabricator.localhost  /etc/nginx/sites-available/phabricator.localhost 
ln -s /phabricator.localhost  /etc/nginx/sites-enabled/phabricator.localhost 

service nginx restart
/etc/init.d/mysql restart

# set the database
/phabricator/bin/config set mysql.host localhost
/phabricator/bin/config set mysql.user root
/phabricator/bin/config set mysql.pass root
/phabricator/bin/storage upgrade --user root --password root --force

# set the base uri
/phabricator/bin/config set phabricator.base-uri 'http://52.59.242.238:8081'

# set git repo support
/phabricator/bin/config set phd.user phd
mkdir -p /var/repo /var/phabricator_files /var/tmp/phd 
chown -R phd:65534 /var/repo /var/tmp/phd 
chown -R www-data:www-data /var/phabricator_files/
/phabricator/bin/config set diffusion.ssh-user git
/phabricator/bin/config set "storage.local-disk.path" "/var/phabricator_files/"
/etc/init.d/php5-fpm restart




/etc/init.d/postfix start
/phabricator/bin/phd start




echo
echo
echo "Install probably worked mostly correctly. Continue with the 'Configuration Guide':";
echo
echo "    https://secure.phabricator.com/book/phabricator/article/configuration_guide/";
echo
echo "You can delete any php5-* stuff that's left over in this directory if you want.";
