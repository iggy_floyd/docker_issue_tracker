#!/bin/bash

# set up MTA
/dockerscriptetchosts.sh
/etc/init.d/postfix restart


confirm() {
  echo "Press RETURN to continue, or ^C to cancel.";
  read -e ignored
}

GIT='git'

LTS="Ubuntu 14.04"
ISSUE=`cat /etc/issue`
if [[ $ISSUE != Ubuntu* ]]
then
  echo "This script is intended for use on Ubuntu, but this system appears";
  echo "to be something else. Your results may vary.";
  echo
#  confirm
elif [[ `expr match "$ISSUE" "$LTS"` -eq ${#LTS} ]]
then
  GIT='git-core'
fi

echo "PHABRICATOR UBUNTU INSTALL SCRIPT";
echo "This script will install Phabricator and all of its core dependencies.";
echo "Run it from the directory you want to install into.";
echo

ROOT=`pwd`
echo "Phabricator will be installed to: ${ROOT}.";
#confirm

echo "Testing sudo..."
sudo true
if [ $? -ne 0 ]
then
  echo "ERROR: You must be able to sudo to run this script.";
  exit 1;
fi;

echo "Installing dependencies: git, apache, mysql, php...";
echo

set +x

#sudo apt-get -qq update
#export DEBIAN_FRONTEND=noninteractive
#sudo apt-get -y install \
#  $GIT mysql-server apache2 dpkg-dev \
#  php5 php5-mysql php5-gd php5-dev php5-curl php-apc php5-cli php5-json

# Enable mod_rewrite
#sudo a2enmod rewrite
export TERM=xterm

sudo apt-get -qq update
echo "mysql-server-5.5 mysql-server/root_password password root" | debconf-set-selections
echo "mysql-server-5.5 mysql-server/root_password_again password root" | debconf-set-selections
apt-get -y install mysql-server-5.5 $GIT dpkg-dev \
  php5 php5-mysql php5-gd php5-dev php5-curl php-apc php5-cli php5-json


if [ ! -e libphutil ]
then
  git clone https://github.com/phacility/libphutil.git
else
  (cd libphutil && git pull --rebase)
fi

if [ ! -e arcanist ]
then
  git clone https://github.com/phacility/arcanist.git
else
  (cd arcanist && git pull --rebase)
fi

if [ ! -e phabricator ]
then
  git clone https://github.com/phacility/phabricator.git
else
  (cd phabricator && git pull --rebase)
fi

# Enable mod_rewrite
sudo a2enmod rewrite
sudo a2enmod headers

sudo mkdir -p /www/public
sudo mkdir -p /www/logs

# add access to the VirtualHost

#sudo sh -c 'echo  " <Directory /www/public>" >> /etc/apache2/apache2.conf'
#sudo sh -c 'echo  "     Options FollowSymLinks " >> /etc/apache2/apache2.conf '
#sudo sh -c 'echo  "     AllowOverride All " >> /etc/apache2/apache2.conf'
#sudo sh -c 'echo  "     Require all granted " >> /etc/apache2/apache2.conf'
#sudo sh -c 'echo  " </Directory>" >> /etc/apache2/apache2.conf'

sudo sh -c 'echo  " <Directory /phabricator/webroot>" >> /etc/apache2/apache2.conf'
sudo sh -c 'echo  "     Options Indexes FollowSymLinks " >> /etc/apache2/apache2.conf '
sudo sh -c 'echo  "     AllowOverride All " >> /etc/apache2/apache2.conf'
sudo sh -c 'echo  "     Require all granted " >> /etc/apache2/apache2.conf'
sudo sh -c 'echo  " </Directory>" >> /etc/apache2/apache2.conf'

# add a new VirtualHost
#cat <<_END >/etc/apache2/sites-available/phabricator.conf 
#<VirtualHost *:80> 
#     ServerAdmin webmaster@address.validator.com
#     ServerName  address.validator.com
#     ServerAlias www.address.validator.com
#     DocumentRoot /www/public
#     ErrorLog /www/logs/error.log 
#     CustomLog /www/logs/access.log combined
#</VirtualHost>
#_END	


cat <<_END >/etc/apache2/sites-available/phabricator.conf 
<VirtualHost *:80> 
# Change this to the domain which points to your host.
  ServerName phabricator.localhost

  # Change this to the path where you put 'phabricator' when you checked it
  # out from GitHub when following the Installation Guide.
  #
  # Make sure you include "/webroot" at the end!
  DocumentRoot /phabricator/webroot
  
#  <Directory "/phabricator/webroot">
#   Order allow,deny
#   Allow from all
#   Options Indexes FollowSymLinks
#   AllowOverride All
#   Require all granted   
#  </Directory>

  RewriteEngine on
  RewriteRule ^/rsrc/(.*)     -                       [L,QSA]
  RewriteRule ^/favicon.ico   -                       [L,QSA]
  RewriteRule ^(.*)$          /index.php?__path__=/\$1  [B,L,QSA]

</VirtualHost>
_END


# add info.php to show available configuration
#cat <<_END >> /www/public/info.php
#<?php
#phpinfo();
#?>
#_END

cat <<_END >> /phabricator/webroot/info.php
<?php
phpinfo();
?>
_END



#cat <<_END >/www/public/index.html
#<html>
#  <head>
#    <title>Test</title>
#  </head>
#  <body>
#    <h1>Success: You Have Set Up a Virtual Host for Phabricator!</h1>
#  </body>
#</html>
#_END

# enable the new VirtualHost
sudo a2ensite phabricator.conf
sudo /etc/init.d/apache2 restart

sudo mv /etc/apache2/sites-enabled/000-default.conf /etc/apache2/sites-enabled/100-default.conf
sudo mv /etc/apache2/sites-enabled/phabricator.conf /etc/apache2/sites-enabled/000-phabricator.conf
sudo /etc/init.d/apache2 restart
sudo /etc/init.d/mysql restart

eval $'cat <<\002\n'"$(</ngnix_forward_proxy)"$'\n\002' > /etc/nginx/sites-enabled/default
service nginx restart



HAVEPCNTL=`php -r "echo extension_loaded('pcntl');"`
if [ $HAVEPCNTL != "1" ]
then
  echo "Installing pcntl...";
  echo
  apt-get source php5
  PHP5=`ls -1F | grep '^php5-.*/$'`
  (cd $PHP5/ext/pcntl && phpize && ./configure && make && sudo make install)
else
  echo "pcntl already installed";
fi

/phabricator/bin/config set mysql.host localhost
/phabricator/bin/config set mysql.user root
/phabricator/bin/config set mysql.pass root
/phabricator/bin/storage upgrade --user root --password root --force



echo
echo
echo "Install probably worked mostly correctly. Continue with the 'Configuration Guide':";
echo
echo "    https://secure.phabricator.com/book/phabricator/article/configuration_guide/";
echo
echo "You can delete any php5-* stuff that's left over in this directory if you want.";
