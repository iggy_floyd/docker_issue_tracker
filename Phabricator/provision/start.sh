#!/bin/bash


sudo /etc/init.d/php5-fpm restart
sudo /etc/init.d/mysql restart
sudo sh -c 'echo  "127.0.0.1       phabricator.localhost" >> /etc/hosts'
mv /etc/nginx/sites-available/default /etc/nginx/sites-available/_default
mv /etc/nginx/sites-enabled/default /etc/nginx/sites-enabled/_default
service nginx restart
service postfix start
/usr/sbin/sshd  -f /etc/ssh/sshd_config.phabricator
/usr/sbin/sshd -D

#while true; do 
#  sleep 1  
#  done
#read
#sudo nginx -g 'daemon off;'


